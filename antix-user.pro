QT       += widgets
CONFIG   += c++17 release warn_on thread qt

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

TEMPLATE = app
TARGET = antix-user

TRANSLATIONS += translations/antix-user_ca.ts \
                translations/antix-user_cs.ts \
                translations/antix-user_da.ts \
                translations/antix-user_de.ts \
                translations/antix-user_el.ts \
                translations/antix-user_es_ES.ts \
                translations/antix-user_es.ts \
                translations/antix-user_fa.ts \
                translations/antix-user_fil_PH.ts \
                translations/antix-user_fi.ts \
                translations/antix-user_fr_BE.ts \
                translations/antix-user_fr.ts \
                translations/antix-user_gl_ES.ts \
                translations/antix-user_he_IL.ts \
                translations/antix-user_hi.ts \
                translations/antix-user_hu.ts \
                translations/antix-user_it.ts \
                translations/antix-user_ja.ts \
		translations/antix-user_nb.ts \
                translations/antix-user_nl.ts \
                translations/antix-user_pl.ts \
                translations/antix-user_pt.ts \
                translations/antix-user_pt_BR.ts \
                translations/antix-user_ru.ts \
                translations/antix-user_ru_RU.ts \
                translations/antix-user_sk.ts \
                translations/antix-user_sl.ts \
                translations/antix-user_sq.ts \
                translations/antix-user_sv.ts \
                translations/antix-user_tr.ts \
                translations/antix-user_uk.ts \
                translations/antix-user_zh_TW.ts

FORMS += \
    mainwindow.ui

HEADERS += \
    common.h \
    mainwindow.h \
    passedit.h \
    cmd.h \
    about.h

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    cmd.cpp \
    about.cpp \
    passedit.cpp

LIBS += -lcrypt -lzxcvbn

RESOURCES += \
    images.qrc
