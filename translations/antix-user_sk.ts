<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="sk">
<context>
    <name>MEConfig</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="973"/>
        <source>User Manager</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <location filename="ui_mainwindow.h" line="1031"/>
        <source>Administration</source>
        <translation>Správa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <location filename="ui_mainwindow.h" line="1033"/>
        <source>Add a new user</source>
        <translation>Pridať nového užívateľa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <location filename="ui_mainwindow.h" line="974"/>
        <source>Add User Account</source>
        <translation>Pridať užívateľské konto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <location filename="ui_mainwindow.h" line="976"/>
        <source>Enter password for new user</source>
        <translation>Zadajte heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <location filename="ui_mainwindow.h" line="979"/>
        <source>password</source>
        <translation>heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <location filename="ui_mainwindow.h" line="981"/>
        <source>Reenter password for new user</source>
        <translation>Znovu zadať heslo nového užívateľa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="984"/>
        <source>confirm password</source>
        <translation>potvrdiť heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <location filename="ui_mainwindow.h" line="986"/>
        <source>Username of new user</source>
        <translation>Uživateľské meno</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="ui_mainwindow.h" line="988"/>
        <source>User login name:</source>
        <translation>Prihlasovacie meno:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <location filename="mainwindow.ui" line="466"/>
        <location filename="ui_mainwindow.h" line="990"/>
        <location filename="ui_mainwindow.h" line="1020"/>
        <source>Enter username of new user</source>
        <translation>Zadajte uživateľské meno</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="993"/>
        <location filename="ui_mainwindow.h" line="1023"/>
        <source>username</source>
        <translation>uživateľské meno</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="995"/>
        <location filename="ui_mainwindow.h" line="999"/>
        <source>Password for new user</source>
        <translation>Heslo nového užívateľa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <location filename="ui_mainwindow.h" line="997"/>
        <source>Confirm user password:</source>
        <translation>potvrdiť heslo:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="ui_mainwindow.h" line="1001"/>
        <source>User password:</source>
        <translation>Užívateľské heslo:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <location filename="ui_mainwindow.h" line="1002"/>
        <source>Grant this user administrative rights to the system (sudo)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="1003"/>
        <source>Delete User Account</source>
        <translation>Zmazať užívateľské konto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="333"/>
        <location filename="ui_mainwindow.h" line="1005"/>
        <source>Select user</source>
        <translation>Vyberte užívateľa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <location filename="mainwindow.ui" line="1436"/>
        <location filename="ui_mainwindow.h" line="1009"/>
        <location filename="ui_mainwindow.h" line="1147"/>
        <source>Select user to delete</source>
        <translation>Zvoľte užívateľa k zmazaniu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="1011"/>
        <source>User to delete:</source>
        <translation>Užívateľ k zmazaniu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="374"/>
        <location filename="ui_mainwindow.h" line="1013"/>
        <source>Also delete the user&apos;s home directory</source>
        <translation>Zároveň zmazať home zložku užívateľa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="377"/>
        <location filename="ui_mainwindow.h" line="1015"/>
        <source>Delete user home directory</source>
        <translation>Zmazať home zložku užívateľa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="1016"/>
        <source>Rename User Account</source>
        <translation>Premenovať užívateľský účet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="mainwindow.ui" line="570"/>
        <location filename="ui_mainwindow.h" line="1017"/>
        <location filename="ui_mainwindow.h" line="1029"/>
        <source>Select user to modify:</source>
        <translation>Zvoľte užívateľa k zmene:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="450"/>
        <location filename="ui_mainwindow.h" line="1018"/>
        <source>New user name:</source>
        <translation>Nové meno používateľa:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <location filename="ui_mainwindow.h" line="1024"/>
        <source>Change User Password</source>
        <translation>Zmeniť užívateľské heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="519"/>
        <location filename="ui_mainwindow.h" line="1026"/>
        <source>new password</source>
        <translation>nové heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <location filename="ui_mainwindow.h" line="1027"/>
        <source>Confirm new password:</source>
        <translation>potvrdiť nové heslo:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <location filename="ui_mainwindow.h" line="1028"/>
        <source>confirm new password</source>
        <translation>potvrdiť nové heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <location filename="ui_mainwindow.h" line="1030"/>
        <source>New user password:</source>
        <translation>Nové užívateľské heslo:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="1067"/>
        <source>Options</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <location filename="ui_mainwindow.h" line="1069"/>
        <source>Repair a user configuration</source>
        <translation>Opraviť konfiguráciu užívateľa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <location filename="mainwindow.ui" line="1393"/>
        <location filename="ui_mainwindow.h" line="1035"/>
        <location filename="ui_mainwindow.h" line="1145"/>
        <source>Modify User Account</source>
        <translation>Zmeniť užívateľské konto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <location filename="mainwindow.ui" line="685"/>
        <location filename="ui_mainwindow.h" line="1037"/>
        <location filename="ui_mainwindow.h" line="1041"/>
        <source>Select user to repair</source>
        <translation>Zvoľte užívateľa k oprave</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <location filename="mainwindow.ui" line="1445"/>
        <location filename="ui_mainwindow.h" line="1039"/>
        <location filename="ui_mainwindow.h" line="1149"/>
        <source>User to change:</source>
        <translation>Užívateľ k zmene:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="717"/>
        <location filename="ui_mainwindow.h" line="1043"/>
        <source>Restore Defaults</source>
        <translation>Obnoviť predvolené</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="738"/>
        <location filename="ui_mainwindow.h" line="1045"/>
        <source>Restore browser configs to antiX defaults</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <location filename="ui_mainwindow.h" line="1050"/>
        <source>Mozilla (Iceweasel or Firefox) configs</source>
        <translation>Konfigurácía Mozilla (Iceweasel alebo Firefox)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <location filename="ui_mainwindow.h" line="1052"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <location filename="ui_mainwindow.h" line="1055"/>
        <source>Restore group memberships to antiX defaults</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="760"/>
        <location filename="ui_mainwindow.h" line="1060"/>
        <source>Group memberships</source>
        <translation>Členstvo skupiny</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="763"/>
        <location filename="ui_mainwindow.h" line="1062"/>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="773"/>
        <location filename="ui_mainwindow.h" line="1064"/>
        <source>Change Autologin Settings</source>
        <translation>Zmeniť nastavenia automatického prihlasovania</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="779"/>
        <location filename="ui_mainwindow.h" line="1065"/>
        <source>Log in automatically</source>
        <translation>Prihlasovať automaticky</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="786"/>
        <location filename="ui_mainwindow.h" line="1066"/>
        <source>Require password to log in</source>
        <translation>Pri prihlásení požadovať heslo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <location filename="ui_mainwindow.h" line="1123"/>
        <source>Copy/Sync</source>
        <translation>Kopírovať/Synchronizovať</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="828"/>
        <location filename="ui_mainwindow.h" line="1071"/>
        <source>Copy Between Desktops</source>
        <translation>Kopírovať plochy</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <location filename="mainwindow.ui" line="922"/>
        <location filename="ui_mainwindow.h" line="1073"/>
        <location filename="ui_mainwindow.h" line="1087"/>
        <source>Select desktop to copy from</source>
        <translation>Vyberte plochu z ktorej sa bude kopírovať</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="1076"/>
        <source>Select to only copy files</source>
        <translation>Vyberte iba skoírovať súbory</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="871"/>
        <location filename="ui_mainwindow.h" line="1078"/>
        <source>Copy only</source>
        <translation>Iba skopírovať</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="887"/>
        <location filename="mainwindow.ui" line="900"/>
        <location filename="ui_mainwindow.h" line="1080"/>
        <location filename="ui_mainwindow.h" line="1083"/>
        <source>Select desktop to copy to</source>
        <translation>Vyberte plochu do ktorej sa bude kopírovať</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <location filename="ui_mainwindow.h" line="1085"/>
        <source>Copy to:</source>
        <translation>Kopírovať do:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="931"/>
        <location filename="ui_mainwindow.h" line="1089"/>
        <source>Copy from:</source>
        <translation>Kopírovať z:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="944"/>
        <location filename="ui_mainwindow.h" line="1091"/>
        <source>Select to copy and then delete differences</source>
        <translation>Zvoľte kopírovať a potom zmazať rozdiely</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="947"/>
        <location filename="ui_mainwindow.h" line="1093"/>
        <source>Sync</source>
        <translation>Synchronizovať</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <location filename="ui_mainwindow.h" line="1096"/>
        <source>Select to copy/sync Shared</source>
        <translation>Zvoľte kopírovať/synchronizovať Zdieľané</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <location filename="ui_mainwindow.h" line="1098"/>
        <source>Shared folder</source>
        <translation>Zdieľaná zložka</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1009"/>
        <location filename="ui_mainwindow.h" line="1100"/>
        <source>Select to copy/sync entire home</source>
        <translation>Zvoľte kopírovať/synchronizovať kompletnú home zložku</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1012"/>
        <location filename="ui_mainwindow.h" line="1102"/>
        <source>Entire home</source>
        <translation>Kopletná home zložka</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1028"/>
        <location filename="ui_mainwindow.h" line="1104"/>
        <source>Select to copy/sync the browser configuration</source>
        <translation>Zvoľte kopírovať/synchronizovať konfiguráciu prehliadača</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1031"/>
        <location filename="ui_mainwindow.h" line="1106"/>
        <source>Mozilla (Firefox or Iceweasel) configs</source>
        <translation>Konfigurácía Mozilla (Iceweasel alebo Firefox)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1044"/>
        <location filename="ui_mainwindow.h" line="1108"/>
        <source>Select to copy/sync Documents</source>
        <translation>Zvoľte kopírovať/synchronizovať zložku Dokumenty</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <location filename="ui_mainwindow.h" line="1110"/>
        <source>Documents folder</source>
        <translation>Zložka Dokumenty</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1054"/>
        <location filename="ui_mainwindow.h" line="1111"/>
        <source>What to copy/sync:</source>
        <translation>Čo sa bude kopírovať/synchronizovať:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1070"/>
        <location filename="ui_mainwindow.h" line="1112"/>
        <source>Progress</source>
        <translation>Progres</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1100"/>
        <location filename="mainwindow.ui" line="1116"/>
        <location filename="ui_mainwindow.h" line="1114"/>
        <location filename="ui_mainwindow.h" line="1117"/>
        <source>Status of the changes</source>
        <translation>Stav zmien</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1119"/>
        <location filename="ui_mainwindow.h" line="1119"/>
        <source>Status:</source>
        <translation>Stav:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1135"/>
        <location filename="ui_mainwindow.h" line="1121"/>
        <source>Progress of the changes</source>
        <translation>Progres zmien</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1172"/>
        <location filename="ui_mainwindow.h" line="1144"/>
        <source>Add/Remove Groups</source>
        <translation>Vytvoriť/Odstrániť Skupiny</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1184"/>
        <location filename="ui_mainwindow.h" line="1124"/>
        <source>Add Group</source>
        <translation>Vytvoriť Skupinu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <location filename="ui_mainwindow.h" line="1126"/>
        <source>Enter name of new group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1217"/>
        <location filename="ui_mainwindow.h" line="1129"/>
        <source>groupname</source>
        <translation>groupname</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1227"/>
        <location filename="ui_mainwindow.h" line="1131"/>
        <source>Create a group with GID &gt; 1000</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1230"/>
        <location filename="ui_mainwindow.h" line="1133"/>
        <source>Create a user-level group</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1259"/>
        <location filename="ui_mainwindow.h" line="1135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter name of new group&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1262"/>
        <location filename="ui_mainwindow.h" line="1137"/>
        <source>Group name:</source>
        <translation>Názov skupiny:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1281"/>
        <location filename="ui_mainwindow.h" line="1138"/>
        <source>Delete Group</source>
        <translation>Odstrániť Skupinu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1350"/>
        <location filename="ui_mainwindow.h" line="1140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select group to delete&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1359"/>
        <location filename="ui_mainwindow.h" line="1142"/>
        <source>Select group to delete:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1366"/>
        <location filename="ui_mainwindow.h" line="1143"/>
        <source>*Please doublecheck your selections before applying, removing a wrong group can break your system.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1381"/>
        <location filename="ui_mainwindow.h" line="1155"/>
        <source>Group Membership</source>
        <translation>Členstvo v skupine</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1455"/>
        <location filename="ui_mainwindow.h" line="1150"/>
        <source>Groups user belongs to (change the groups by selecting/deselecting the appropriate boxes):</source>
        <translation>Skupiny ku ktorým je užívateľ priradený (skupiny zmeníte vybratím/odvybratím odpovedajúcich políčok):</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1478"/>
        <location filename="ui_mainwindow.h" line="1151"/>
        <source>*Please doublecheck your selections before applying, assigning wrong group memberships can break your system. If you made a mistake, use restore group membership in Options tab to restore the defaults.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <location filename="ui_mainwindow.h" line="1153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select user to change&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1583"/>
        <location filename="ui_mainwindow.h" line="1157"/>
        <source>Cancel any changes then quit</source>
        <translation>Zrušit všetky zmeny a ukončiť</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1586"/>
        <location filename="ui_mainwindow.h" line="1159"/>
        <source>Close</source>
        <translation>Zatvoriť</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1593"/>
        <location filename="ui_mainwindow.h" line="1161"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1634"/>
        <location filename="ui_mainwindow.h" line="1165"/>
        <source>Display help </source>
        <translation>Zobraziť nápovedu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1637"/>
        <location filename="ui_mainwindow.h" line="1167"/>
        <source>Help</source>
        <translation>Pomocník</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1644"/>
        <location filename="ui_mainwindow.h" line="1169"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1651"/>
        <location filename="ui_mainwindow.h" line="1172"/>
        <source>Apply any changes</source>
        <translation>Použiť všetky zmeny</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1654"/>
        <location filename="ui_mainwindow.h" line="1174"/>
        <source>Apply</source>
        <translation>Použiť</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1661"/>
        <location filename="ui_mainwindow.h" line="1176"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1668"/>
        <location filename="ui_mainwindow.h" line="1179"/>
        <source>About this application</source>
        <translation>O tejto aplikácii</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="ui_mainwindow.h" line="1181"/>
        <source>About...</source>
        <translation>O Programe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1678"/>
        <location filename="ui_mainwindow.h" line="1183"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="166"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="201"/>
        <source>none</source>
        <translation>nič</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <source>browse...</source>
        <translation>prehľadať...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>The user configuration will be repaired. Please close all other applications now. When finished, please logout or reboot. Are you sure you want to repair now?</source>
        <translation>Konfigurácia užívateľa bude opravená. Prosím zavrite všetky bežiace aplikácie. Po skončení operácie sa prosím odhláste alebo reštartujte PC. Skutočne si prajete spustiť opravu?  </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>User group membership was restored.</source>
        <translation>Členstvo v skupine užívateľov bolo obnovené.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Mozilla settings were reset.</source>
        <translation>Nastavenia Mozilly boli resetované.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Autologin options</source>
        <translation>Možnosti automatického prihlasovania</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="257"/>
        <source>Autologin has been disabled for the &apos;%1&apos; account.</source>
        <translation>Automatické prihlásenie pre užívateľské konto &apos;%1&apos; bolo vypnuté. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Autologin has been enabled for the &apos;%1&apos; account.</source>
        <translation>Automatické prihlásenie pre užívateľské konto &apos;%1&apos; bolo zapnuté. </translation>
    </message>
</context>
<context>
    <name>PassEdit</name>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Negligible</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Very weak</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Weak</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Moderate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Strong</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Very strong</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="164"/>
        <source>Password strength: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Hide the password</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Show the password</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="50"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <location filename="about.cpp" line="61"/>
        <source>Changelog</source>
        <translation>História zmien</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Zatvoriť</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <location filename="main.cpp" line="73"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>You must run this program with admin access.</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>