<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="sv">
<context>
    <name>MEConfig</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="973"/>
        <source>User Manager</source>
        <translation>Användarhanterare</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <location filename="ui_mainwindow.h" line="1031"/>
        <source>Administration</source>
        <translation>Administration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <location filename="ui_mainwindow.h" line="1033"/>
        <source>Add a new user</source>
        <translation>Lägg till en ny användare</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <location filename="ui_mainwindow.h" line="974"/>
        <source>Add User Account</source>
        <translation>Lägg till Användar-konto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <location filename="ui_mainwindow.h" line="976"/>
        <source>Enter password for new user</source>
        <translation>Skriv in lösenord för ny användare</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <location filename="ui_mainwindow.h" line="979"/>
        <source>password</source>
        <translation>lösenord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <location filename="ui_mainwindow.h" line="981"/>
        <source>Reenter password for new user</source>
        <translation>Skriv in den nya användarens lösenord en gång till</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="984"/>
        <source>confirm password</source>
        <translation>bekräfta lösenord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <location filename="ui_mainwindow.h" line="986"/>
        <source>Username of new user</source>
        <translation>Ny användares användar-namn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="ui_mainwindow.h" line="988"/>
        <source>User login name:</source>
        <translation>Användares inloggningsnamn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <location filename="mainwindow.ui" line="466"/>
        <location filename="ui_mainwindow.h" line="990"/>
        <location filename="ui_mainwindow.h" line="1020"/>
        <source>Enter username of new user</source>
        <translation>Skriv in den nya användarens användar-namn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="993"/>
        <location filename="ui_mainwindow.h" line="1023"/>
        <source>username</source>
        <translation>användar-namn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="995"/>
        <location filename="ui_mainwindow.h" line="999"/>
        <source>Password for new user</source>
        <translation>Lösenord för ny användare</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <location filename="ui_mainwindow.h" line="997"/>
        <source>Confirm user password:</source>
        <translation>Bekräfta användar-lösenord:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="ui_mainwindow.h" line="1001"/>
        <source>User password:</source>
        <translation>Användar-lösenord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <location filename="ui_mainwindow.h" line="1002"/>
        <source>Grant this user administrative rights to the system (sudo)</source>
        <translation>Ge denna användare administrativa rättigheter till systemet (sudo)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="1003"/>
        <source>Delete User Account</source>
        <translation>Ta bort Användar-konto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="333"/>
        <location filename="ui_mainwindow.h" line="1005"/>
        <source>Select user</source>
        <translation>Välj användare</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <location filename="mainwindow.ui" line="1436"/>
        <location filename="ui_mainwindow.h" line="1009"/>
        <location filename="ui_mainwindow.h" line="1147"/>
        <source>Select user to delete</source>
        <translation>Välj vilken användare som ska tas bort</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="1011"/>
        <source>User to delete:</source>
        <translation>Användare att ta bort:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="374"/>
        <location filename="ui_mainwindow.h" line="1013"/>
        <source>Also delete the user&apos;s home directory</source>
        <translation>Ta även bort användarens hemkatalog</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="377"/>
        <location filename="ui_mainwindow.h" line="1015"/>
        <source>Delete user home directory</source>
        <translation>Ta bort användarens hemkatalog</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="1016"/>
        <source>Rename User Account</source>
        <translation>Byt namn på Användarkonto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="mainwindow.ui" line="570"/>
        <location filename="ui_mainwindow.h" line="1017"/>
        <location filename="ui_mainwindow.h" line="1029"/>
        <source>Select user to modify:</source>
        <translation>Välj användare som ska modifieras:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="450"/>
        <location filename="ui_mainwindow.h" line="1018"/>
        <source>New user name:</source>
        <translation>Nytt användarnamn:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <location filename="ui_mainwindow.h" line="1024"/>
        <source>Change User Password</source>
        <translation>Byt användarlösenord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="519"/>
        <location filename="ui_mainwindow.h" line="1026"/>
        <source>new password</source>
        <translation>nytt lösenord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <location filename="ui_mainwindow.h" line="1027"/>
        <source>Confirm new password:</source>
        <translation>Bekräfta nytt lösenord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <location filename="ui_mainwindow.h" line="1028"/>
        <source>confirm new password</source>
        <translation>bekräfta nytt lösenord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <location filename="ui_mainwindow.h" line="1030"/>
        <source>New user password:</source>
        <translation>Nytt användarlösenord:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="1067"/>
        <source>Options</source>
        <translation>Valmöjligheter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <location filename="ui_mainwindow.h" line="1069"/>
        <source>Repair a user configuration</source>
        <translation>Reparera en användar-konfiguration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <location filename="mainwindow.ui" line="1393"/>
        <location filename="ui_mainwindow.h" line="1035"/>
        <location filename="ui_mainwindow.h" line="1145"/>
        <source>Modify User Account</source>
        <translation>Modifiera Användarkonto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <location filename="mainwindow.ui" line="685"/>
        <location filename="ui_mainwindow.h" line="1037"/>
        <location filename="ui_mainwindow.h" line="1041"/>
        <source>Select user to repair</source>
        <translation>Välj användare att reparera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <location filename="mainwindow.ui" line="1445"/>
        <location filename="ui_mainwindow.h" line="1039"/>
        <location filename="ui_mainwindow.h" line="1149"/>
        <source>User to change:</source>
        <translation>Användare att ändra:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="717"/>
        <location filename="ui_mainwindow.h" line="1043"/>
        <source>Restore Defaults</source>
        <translation>Återställ till Standard</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="738"/>
        <location filename="ui_mainwindow.h" line="1045"/>
        <source>Restore browser configs to antiX defaults</source>
        <translation>Återställ webbläsar-konfigurationen till antiX standard </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <location filename="ui_mainwindow.h" line="1050"/>
        <source>Mozilla (Iceweasel or Firefox) configs</source>
        <translation>Mozilla (Iceweasel eller Firefox) konfiguration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <location filename="ui_mainwindow.h" line="1052"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <location filename="ui_mainwindow.h" line="1055"/>
        <source>Restore group memberships to antiX defaults</source>
        <translation>Återställ grupp-medlemskapen till antiX standard</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="760"/>
        <location filename="ui_mainwindow.h" line="1060"/>
        <source>Group memberships</source>
        <translation>Grupp-medlemskap</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="763"/>
        <location filename="ui_mainwindow.h" line="1062"/>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="773"/>
        <location filename="ui_mainwindow.h" line="1064"/>
        <source>Change Autologin Settings</source>
        <translation>Ändra Autologin-inställlningar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="779"/>
        <location filename="ui_mainwindow.h" line="1065"/>
        <source>Log in automatically</source>
        <translation>Logga in automatiskt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="786"/>
        <location filename="ui_mainwindow.h" line="1066"/>
        <source>Require password to log in</source>
        <translation>Kräv lösenord för att logga in</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <location filename="ui_mainwindow.h" line="1123"/>
        <source>Copy/Sync</source>
        <translation>Kopiera/Synkronisera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="828"/>
        <location filename="ui_mainwindow.h" line="1071"/>
        <source>Copy Between Desktops</source>
        <translation>Kopiera mellan Skrivbord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <location filename="mainwindow.ui" line="922"/>
        <location filename="ui_mainwindow.h" line="1073"/>
        <location filename="ui_mainwindow.h" line="1087"/>
        <source>Select desktop to copy from</source>
        <translation>Välj skrivbord att kopiera från</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="1076"/>
        <source>Select to only copy files</source>
        <translation>Välj att endast kopiera filer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="871"/>
        <location filename="ui_mainwindow.h" line="1078"/>
        <source>Copy only</source>
        <translation>Endast kopiera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="887"/>
        <location filename="mainwindow.ui" line="900"/>
        <location filename="ui_mainwindow.h" line="1080"/>
        <location filename="ui_mainwindow.h" line="1083"/>
        <source>Select desktop to copy to</source>
        <translation>Välj skrivbord att kopiera till</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <location filename="ui_mainwindow.h" line="1085"/>
        <source>Copy to:</source>
        <translation>Kopiera till:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="931"/>
        <location filename="ui_mainwindow.h" line="1089"/>
        <source>Copy from:</source>
        <translation>Kopiera från:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="944"/>
        <location filename="ui_mainwindow.h" line="1091"/>
        <source>Select to copy and then delete differences</source>
        <translation>Välj att kopiera och sedan ta bort skillnader</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="947"/>
        <location filename="ui_mainwindow.h" line="1093"/>
        <source>Sync</source>
        <translation>Synkronisera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <location filename="ui_mainwindow.h" line="1096"/>
        <source>Select to copy/sync Shared</source>
        <translation>Välj att kopiera/synkronisera Shared</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <location filename="ui_mainwindow.h" line="1098"/>
        <source>Shared folder</source>
        <translation>Delad katalog</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1009"/>
        <location filename="ui_mainwindow.h" line="1100"/>
        <source>Select to copy/sync entire home</source>
        <translation>Välj att kopiera/synkronisera hela home</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1012"/>
        <location filename="ui_mainwindow.h" line="1102"/>
        <source>Entire home</source>
        <translation>Hela home</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1028"/>
        <location filename="ui_mainwindow.h" line="1104"/>
        <source>Select to copy/sync the browser configuration</source>
        <translation>Välj att kopiera/synkronisera webbläsarkonfigurationen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1031"/>
        <location filename="ui_mainwindow.h" line="1106"/>
        <source>Mozilla (Firefox or Iceweasel) configs</source>
        <translation>Mozilla (Firefox eller Iceweasel) konfiguration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1044"/>
        <location filename="ui_mainwindow.h" line="1108"/>
        <source>Select to copy/sync Documents</source>
        <translation>Välj att kopiera/synkronisera Documents</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <location filename="ui_mainwindow.h" line="1110"/>
        <source>Documents folder</source>
        <translation>Documents-katalog</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1054"/>
        <location filename="ui_mainwindow.h" line="1111"/>
        <source>What to copy/sync:</source>
        <translation>Vad att kopiera/synkronisera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1070"/>
        <location filename="ui_mainwindow.h" line="1112"/>
        <source>Progress</source>
        <translation>Framsteg</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1100"/>
        <location filename="mainwindow.ui" line="1116"/>
        <location filename="ui_mainwindow.h" line="1114"/>
        <location filename="ui_mainwindow.h" line="1117"/>
        <source>Status of the changes</source>
        <translation>Ändringarnas status</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1119"/>
        <location filename="ui_mainwindow.h" line="1119"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1135"/>
        <location filename="ui_mainwindow.h" line="1121"/>
        <source>Progress of the changes</source>
        <translation>Ändringarnas framsteg</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1172"/>
        <location filename="ui_mainwindow.h" line="1144"/>
        <source>Add/Remove Groups</source>
        <translation>Lägg till/Ta bort Grupper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1184"/>
        <location filename="ui_mainwindow.h" line="1124"/>
        <source>Add Group</source>
        <translation>Lägg till Grupp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <location filename="ui_mainwindow.h" line="1126"/>
        <source>Enter name of new group</source>
        <translation>Skriv in namnet på den nya gruppen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1217"/>
        <location filename="ui_mainwindow.h" line="1129"/>
        <source>groupname</source>
        <translation>Gruppnamn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1227"/>
        <location filename="ui_mainwindow.h" line="1131"/>
        <source>Create a group with GID &gt; 1000</source>
        <translation>Skapa en grupp med GID &gt; 1000</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1230"/>
        <location filename="ui_mainwindow.h" line="1133"/>
        <source>Create a user-level group</source>
        <translation>Skapa en användarnivå-grupp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1259"/>
        <location filename="ui_mainwindow.h" line="1135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter name of new group&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Skriv namnet på den nya gruppen&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1262"/>
        <location filename="ui_mainwindow.h" line="1137"/>
        <source>Group name:</source>
        <translation>Grupp namn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1281"/>
        <location filename="ui_mainwindow.h" line="1138"/>
        <source>Delete Group</source>
        <translation>Ta bort Grupp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1350"/>
        <location filename="ui_mainwindow.h" line="1140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select group to delete&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Välj grupp att ta bort&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1359"/>
        <location filename="ui_mainwindow.h" line="1142"/>
        <source>Select group to delete:</source>
        <translation>Välj grupp att ta bort:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1366"/>
        <location filename="ui_mainwindow.h" line="1143"/>
        <source>*Please doublecheck your selections before applying, removing a wrong group can break your system.</source>
        <translation>*Var vänlig dubbelkolla dina val innan du  tillämpar, att ta bort fel grupp kan förstöra ditt system.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1381"/>
        <location filename="ui_mainwindow.h" line="1155"/>
        <source>Group Membership</source>
        <translation>Grupp-medlemskap</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1455"/>
        <location filename="ui_mainwindow.h" line="1150"/>
        <source>Groups user belongs to (change the groups by selecting/deselecting the appropriate boxes):</source>
        <translation>Grupper användaren tillhör (ändra grupperna genom att markera/avmarkera respektive rutor):</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1478"/>
        <location filename="ui_mainwindow.h" line="1151"/>
        <source>*Please doublecheck your selections before applying, assigning wrong group memberships can break your system. If you made a mistake, use restore group membership in Options tab to restore the defaults.</source>
        <translation>*Var vänlig dubbelkolla dina val innan du genomför, tilldelande av fel gruppmedlemskap kan förstöra ditt system. Om du gjort ett misstag, använd återställ gruppmedlemskap i Valmöjlighetsfliken för att återställa till standard.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <location filename="ui_mainwindow.h" line="1153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select user to change&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Välj användare att ändra&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1583"/>
        <location filename="ui_mainwindow.h" line="1157"/>
        <source>Cancel any changes then quit</source>
        <translation>Avbryt ändringarna och sedan sluta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1586"/>
        <location filename="ui_mainwindow.h" line="1159"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1593"/>
        <location filename="ui_mainwindow.h" line="1161"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1634"/>
        <location filename="ui_mainwindow.h" line="1165"/>
        <source>Display help </source>
        <translation>Visa hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1637"/>
        <location filename="ui_mainwindow.h" line="1167"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1644"/>
        <location filename="ui_mainwindow.h" line="1169"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1651"/>
        <location filename="ui_mainwindow.h" line="1172"/>
        <source>Apply any changes</source>
        <translation>Genomför ändringar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1654"/>
        <location filename="ui_mainwindow.h" line="1174"/>
        <source>Apply</source>
        <translation>Använd</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1661"/>
        <location filename="ui_mainwindow.h" line="1176"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1668"/>
        <location filename="ui_mainwindow.h" line="1179"/>
        <source>About this application</source>
        <translation>Om detta program</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="ui_mainwindow.h" line="1181"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1678"/>
        <location filename="ui_mainwindow.h" line="1183"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="166"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="201"/>
        <source>none</source>
        <translation>ingen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <source>browse...</source>
        <translation>bläddra...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>The user configuration will be repaired. Please close all other applications now. When finished, please logout or reboot. Are you sure you want to repair now?</source>
        <translation>Användar-konfigurationen kommer att repareras. Var vänlig stäng alla andra program nu. När det är klart, logga ut eller starta om. Är du säker på att du vill reparera nu?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>User group membership was restored.</source>
        <translation>Användargrupp-medlemskap återställdes.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Mozilla settings were reset.</source>
        <translation>Mozilla inställningar återställdes.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Autologin options</source>
        <translation>Autologin-alternativ</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="257"/>
        <source>Autologin has been disabled for the &apos;%1&apos; account.</source>
        <translation>Autologin har stängts av för &apos;%1&apos; kontot.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Autologin has been enabled for the &apos;%1&apos; account.</source>
        <translation>Autologin har möjliggjorts för &apos;%1&apos; kontot.</translation>
    </message>
</context>
<context>
    <name>PassEdit</name>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Negligible</source>
        <translation>Obetydlig</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Very weak</source>
        <translation>Mycket svag</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Weak</source>
        <translation>Svag</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Moderate</source>
        <translation>Mittemellan</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Strong</source>
        <translation>Stark</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Very strong</source>
        <translation>Mycket stark</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="164"/>
        <source>Password strength: %1</source>
        <translation>Lösenords-styrka: %1</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Hide the password</source>
        <translation>Dölj lösenordet</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Show the password</source>
        <translation>Visa lösenordet</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="50"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <location filename="about.cpp" line="61"/>
        <source>Changelog</source>
        <translation>Ändringslogg</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Stäng</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <location filename="main.cpp" line="73"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Du verkar vara inloggad som root, var vänlig logga ut och logga in som vanlig användare för att använda detta program.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>You must run this program with admin access.</source>
        <translation>Du måste köra detta program med administratörsrättigheter.</translation>
    </message>
</context>
</TS>