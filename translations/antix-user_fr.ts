<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="fr">
<context>
    <name>MEConfig</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="973"/>
        <source>User Manager</source>
        <translation>Gestionnaire des utilisateurs - User Manager</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <location filename="ui_mainwindow.h" line="1031"/>
        <source>Administration</source>
        <translation>Administration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <location filename="ui_mainwindow.h" line="1033"/>
        <source>Add a new user</source>
        <translation>Ajouter un nouvel utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <location filename="ui_mainwindow.h" line="974"/>
        <source>Add User Account</source>
        <translation>Ajouter un compte utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <location filename="ui_mainwindow.h" line="976"/>
        <source>Enter password for new user</source>
        <translation>Entrez le mot de passe du nouvel utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <location filename="ui_mainwindow.h" line="979"/>
        <source>password</source>
        <translation>mot de passe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <location filename="ui_mainwindow.h" line="981"/>
        <source>Reenter password for new user</source>
        <translation>Ressaisissez le mot de passe du nouvel utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="984"/>
        <source>confirm password</source>
        <translation>Confirmer le mot de passe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <location filename="ui_mainwindow.h" line="986"/>
        <source>Username of new user</source>
        <translation>Nom du nouvel utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="ui_mainwindow.h" line="988"/>
        <source>User login name:</source>
        <translation>Nom de connexion - login - de l’utilisateur :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <location filename="mainwindow.ui" line="466"/>
        <location filename="ui_mainwindow.h" line="990"/>
        <location filename="ui_mainwindow.h" line="1020"/>
        <source>Enter username of new user</source>
        <translation>Entrez le nom du nouvel utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="993"/>
        <location filename="ui_mainwindow.h" line="1023"/>
        <source>username</source>
        <translation>nom d’utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="995"/>
        <location filename="ui_mainwindow.h" line="999"/>
        <source>Password for new user</source>
        <translation>Mot de passe du nouvel utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <location filename="ui_mainwindow.h" line="997"/>
        <source>Confirm user password:</source>
        <translation>Confirmer le mot de passe de l’utilisateur :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="ui_mainwindow.h" line="1001"/>
        <source>User password:</source>
        <translation>Mot de passe de l’utilisateur :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <location filename="ui_mainwindow.h" line="1002"/>
        <source>Grant this user administrative rights to the system (sudo)</source>
        <translation>Accorder à cet utilisateur les droits d’administration sur le système (sudo)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="1003"/>
        <source>Delete User Account</source>
        <translation>Effacer un compte utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="333"/>
        <location filename="ui_mainwindow.h" line="1005"/>
        <source>Select user</source>
        <translation>Choisir l’utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <location filename="mainwindow.ui" line="1436"/>
        <location filename="ui_mainwindow.h" line="1009"/>
        <location filename="ui_mainwindow.h" line="1147"/>
        <source>Select user to delete</source>
        <translation>Choisir l’utilisateur à supprimer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="1011"/>
        <source>User to delete:</source>
        <translation>Utilisateur à supprimer :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="374"/>
        <location filename="ui_mainwindow.h" line="1013"/>
        <source>Also delete the user&apos;s home directory</source>
        <translation>Supprimer également le dossier personnel « home » de l’utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="377"/>
        <location filename="ui_mainwindow.h" line="1015"/>
        <source>Delete user home directory</source>
        <translation>Supprimer le dossier personnel « home » de l’utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="1016"/>
        <source>Rename User Account</source>
        <translation>Renommer un compte utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="mainwindow.ui" line="570"/>
        <location filename="ui_mainwindow.h" line="1017"/>
        <location filename="ui_mainwindow.h" line="1029"/>
        <source>Select user to modify:</source>
        <translation>Choisir l’utilisateur à modifier :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="450"/>
        <location filename="ui_mainwindow.h" line="1018"/>
        <source>New user name:</source>
        <translation>Nouveau nom d’utilisateur :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <location filename="ui_mainwindow.h" line="1024"/>
        <source>Change User Password</source>
        <translation>Changer le mot de passe utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="519"/>
        <location filename="ui_mainwindow.h" line="1026"/>
        <source>new password</source>
        <translation>Nouveau mot de passe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <location filename="ui_mainwindow.h" line="1027"/>
        <source>Confirm new password:</source>
        <translation>Confirmer le nouveau mot de passe :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <location filename="ui_mainwindow.h" line="1028"/>
        <source>confirm new password</source>
        <translation>Confirmer le nouveau mot de passe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <location filename="ui_mainwindow.h" line="1030"/>
        <source>New user password:</source>
        <translation>Nouveau mot de passe de l’utilisateur :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="1067"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <location filename="ui_mainwindow.h" line="1069"/>
        <source>Repair a user configuration</source>
        <translation>Réparer une configuration utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <location filename="mainwindow.ui" line="1393"/>
        <location filename="ui_mainwindow.h" line="1035"/>
        <location filename="ui_mainwindow.h" line="1145"/>
        <source>Modify User Account</source>
        <translation>Modifier le compte utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <location filename="mainwindow.ui" line="685"/>
        <location filename="ui_mainwindow.h" line="1037"/>
        <location filename="ui_mainwindow.h" line="1041"/>
        <source>Select user to repair</source>
        <translation>Choisir l’utilisateur à modifier ou à réparer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <location filename="mainwindow.ui" line="1445"/>
        <location filename="ui_mainwindow.h" line="1039"/>
        <location filename="ui_mainwindow.h" line="1149"/>
        <source>User to change:</source>
        <translation>Utilisateur devant être modifié :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="717"/>
        <location filename="ui_mainwindow.h" line="1043"/>
        <source>Restore Defaults</source>
        <translation>Rétablir la configuration d’origine</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="738"/>
        <location filename="ui_mainwindow.h" line="1045"/>
        <source>Restore browser configs to antiX defaults</source>
        <translation>Restaurer les configurations du navigateur aux valeurs par défaut d’antiX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <location filename="ui_mainwindow.h" line="1050"/>
        <source>Mozilla (Iceweasel or Firefox) configs</source>
        <translation>Configurations de Mozilla (Iceweasel ou Firefox) </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <location filename="ui_mainwindow.h" line="1052"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <location filename="ui_mainwindow.h" line="1055"/>
        <source>Restore group memberships to antiX defaults</source>
        <translation>Restaurer les membres du groupe aux valeurs par défaut d’antiX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="760"/>
        <location filename="ui_mainwindow.h" line="1060"/>
        <source>Group memberships</source>
        <translation>Groupes d’utilisateurs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="763"/>
        <location filename="ui_mainwindow.h" line="1062"/>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="773"/>
        <location filename="ui_mainwindow.h" line="1064"/>
        <source>Change Autologin Settings</source>
        <translation>Modifier les paramètres de l’autologin</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="779"/>
        <location filename="ui_mainwindow.h" line="1065"/>
        <source>Log in automatically</source>
        <translation>Se connecter automatiquement</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="786"/>
        <location filename="ui_mainwindow.h" line="1066"/>
        <source>Require password to log in</source>
        <translation>Mot de passe requis pour se connecter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <location filename="ui_mainwindow.h" line="1123"/>
        <source>Copy/Sync</source>
        <translation>Copie et Synchronisation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="828"/>
        <location filename="ui_mainwindow.h" line="1071"/>
        <source>Copy Between Desktops</source>
        <translation>Copie entre bureaux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <location filename="mainwindow.ui" line="922"/>
        <location filename="ui_mainwindow.h" line="1073"/>
        <location filename="ui_mainwindow.h" line="1087"/>
        <source>Select desktop to copy from</source>
        <translation>Sélectionner le bureau à copier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="1076"/>
        <source>Select to only copy files</source>
        <translation>Sélectionner pour uniquement copier les fichiers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="871"/>
        <location filename="ui_mainwindow.h" line="1078"/>
        <source>Copy only</source>
        <translation>Copie seule</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="887"/>
        <location filename="mainwindow.ui" line="900"/>
        <location filename="ui_mainwindow.h" line="1080"/>
        <location filename="ui_mainwindow.h" line="1083"/>
        <source>Select desktop to copy to</source>
        <translation>Sélectionner le bureau de destination</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <location filename="ui_mainwindow.h" line="1085"/>
        <source>Copy to:</source>
        <translation>Copier vers :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="931"/>
        <location filename="ui_mainwindow.h" line="1089"/>
        <source>Copy from:</source>
        <translation>Copier de :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="944"/>
        <location filename="ui_mainwindow.h" line="1091"/>
        <source>Select to copy and then delete differences</source>
        <translation>Sélectionner pour copier puis supprimer les différences</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="947"/>
        <location filename="ui_mainwindow.h" line="1093"/>
        <source>Sync</source>
        <translation>Synchroniser</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <location filename="ui_mainwindow.h" line="1096"/>
        <source>Select to copy/sync Shared</source>
        <translation>Sélectionner pour copier et synchroniser le répertoire partagé</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <location filename="ui_mainwindow.h" line="1098"/>
        <source>Shared folder</source>
        <translation>Répertoire partagé</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1009"/>
        <location filename="ui_mainwindow.h" line="1100"/>
        <source>Select to copy/sync entire home</source>
        <translation>Sélectionner pour copier et synchroniser le répertoire home en entier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1012"/>
        <location filename="ui_mainwindow.h" line="1102"/>
        <source>Entire home</source>
        <translation>Home en entier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1028"/>
        <location filename="ui_mainwindow.h" line="1104"/>
        <source>Select to copy/sync the browser configuration</source>
        <translation>Sélectionner pour copier et synchroniser la configuration du navigateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1031"/>
        <location filename="ui_mainwindow.h" line="1106"/>
        <source>Mozilla (Firefox or Iceweasel) configs</source>
        <translation>Configurations de Mozilla (Firefox ou Iceweasel)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1044"/>
        <location filename="ui_mainwindow.h" line="1108"/>
        <source>Select to copy/sync Documents</source>
        <translation>Sélectionner pour copier et synchroniser le répertoire Documents</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <location filename="ui_mainwindow.h" line="1110"/>
        <source>Documents folder</source>
        <translation>Répertoire Documents</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1054"/>
        <location filename="ui_mainwindow.h" line="1111"/>
        <source>What to copy/sync:</source>
        <translation>Que faut-il copier et synchoniser :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1070"/>
        <location filename="ui_mainwindow.h" line="1112"/>
        <source>Progress</source>
        <translation>Avancement</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1100"/>
        <location filename="mainwindow.ui" line="1116"/>
        <location filename="ui_mainwindow.h" line="1114"/>
        <location filename="ui_mainwindow.h" line="1117"/>
        <source>Status of the changes</source>
        <translation>Statut des modifications</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1119"/>
        <location filename="ui_mainwindow.h" line="1119"/>
        <source>Status:</source>
        <translation>Statut :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1135"/>
        <location filename="ui_mainwindow.h" line="1121"/>
        <source>Progress of the changes</source>
        <translation>Niveau d’avancement des modifications</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1172"/>
        <location filename="ui_mainwindow.h" line="1144"/>
        <source>Add/Remove Groups</source>
        <translation>Ajouter ou supprimer des groupes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1184"/>
        <location filename="ui_mainwindow.h" line="1124"/>
        <source>Add Group</source>
        <translation>Ajouter un groupe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <location filename="ui_mainwindow.h" line="1126"/>
        <source>Enter name of new group</source>
        <translation>Entrer le nom du nouveau groupe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1217"/>
        <location filename="ui_mainwindow.h" line="1129"/>
        <source>groupname</source>
        <translation>Nom du groupe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1227"/>
        <location filename="ui_mainwindow.h" line="1131"/>
        <source>Create a group with GID &gt; 1000</source>
        <translation>Créer un groupe avec GID &gt; 1000</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1230"/>
        <location filename="ui_mainwindow.h" line="1133"/>
        <source>Create a user-level group</source>
        <translation>Créer un groupe avec un GID dans la plage utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1259"/>
        <location filename="ui_mainwindow.h" line="1135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter name of new group&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Saisir le nom du nouveau groupe&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1262"/>
        <location filename="ui_mainwindow.h" line="1137"/>
        <source>Group name:</source>
        <translation>Nom du groupe :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1281"/>
        <location filename="ui_mainwindow.h" line="1138"/>
        <source>Delete Group</source>
        <translation>Supprimer un groupe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1350"/>
        <location filename="ui_mainwindow.h" line="1140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select group to delete&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Choisir le groupe à supprimer&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1359"/>
        <location filename="ui_mainwindow.h" line="1142"/>
        <source>Select group to delete:</source>
        <translation>Choisir le groupe à supprimer :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1366"/>
        <location filename="ui_mainwindow.h" line="1143"/>
        <source>*Please doublecheck your selections before applying, removing a wrong group can break your system.</source>
        <translation>*Veuillez vérifier deux fois vos sélections avant de les appliquer, la suppression d’un mauvais groupe peut casser votre système.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1381"/>
        <location filename="ui_mainwindow.h" line="1155"/>
        <source>Group Membership</source>
        <translation>Appartenance à un groupe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1455"/>
        <location filename="ui_mainwindow.h" line="1150"/>
        <source>Groups user belongs to (change the groups by selecting/deselecting the appropriate boxes):</source>
        <translation>Groupes auxquels l’utilisateur appartient (changer les groupes en cochant ou décochant les cases appropriées) :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1478"/>
        <location filename="ui_mainwindow.h" line="1151"/>
        <source>*Please doublecheck your selections before applying, assigning wrong group memberships can break your system. If you made a mistake, use restore group membership in Options tab to restore the defaults.</source>
        <translation>* Veuillez vérifier vos sélections avant de les appliquer car l’attribution d’une mauvaise appartenance à un groupe peut endommager votre système. Si vous avez fait une erreur, utilisez la fonction de restauration de l’appartenance à un groupe dans l’onglet Options pour rétablir les valeurs par défaut.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <location filename="ui_mainwindow.h" line="1153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select user to change&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Choisir l’utilisateur à modifier&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1583"/>
        <location filename="ui_mainwindow.h" line="1157"/>
        <source>Cancel any changes then quit</source>
        <translation>Annuler toutes les modifications puis quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1586"/>
        <location filename="ui_mainwindow.h" line="1159"/>
        <source>Close</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1593"/>
        <location filename="ui_mainwindow.h" line="1161"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1634"/>
        <location filename="ui_mainwindow.h" line="1165"/>
        <source>Display help </source>
        <translation>Afficher l’aide </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1637"/>
        <location filename="ui_mainwindow.h" line="1167"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1644"/>
        <location filename="ui_mainwindow.h" line="1169"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1651"/>
        <location filename="ui_mainwindow.h" line="1172"/>
        <source>Apply any changes</source>
        <translation>Appliquer les changements</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1654"/>
        <location filename="ui_mainwindow.h" line="1174"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1661"/>
        <location filename="ui_mainwindow.h" line="1176"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1668"/>
        <location filename="ui_mainwindow.h" line="1179"/>
        <source>About this application</source>
        <translation>À propos de cette application</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="ui_mainwindow.h" line="1181"/>
        <source>About...</source>
        <translation>À propos...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1678"/>
        <location filename="ui_mainwindow.h" line="1183"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="166"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="201"/>
        <source>none</source>
        <translation>aucun</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <source>browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>The user configuration will be repaired. Please close all other applications now. When finished, please logout or reboot. Are you sure you want to repair now?</source>
        <translation>La configuration de l’utilisateur va être modifiée. Veuillez fermer toutes les applications ouvertes. Après exécution, veuillez fermer votre session ou redémarrer votre machine. Êtes-vous sûr de vouloir modifier cette configuration maintenant ?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>User group membership was restored.</source>
        <translation>L’appartenance à un groupe d’utilisateurs a été rétablie.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Mozilla settings were reset.</source>
        <translation>Les paramètres de Mozilla ont été réinitialisés.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Autologin options</source>
        <translation>Options de l’autologin</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="257"/>
        <source>Autologin has been disabled for the &apos;%1&apos; account.</source>
        <translation>L’autologin a été désactivé pour le compte « %1 ».</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Autologin has been enabled for the &apos;%1&apos; account.</source>
        <translation>L’autologin a été activé pour le compte « %1 ».</translation>
    </message>
</context>
<context>
    <name>PassEdit</name>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Negligible</source>
        <translation>Négligeable</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Very weak</source>
        <translation>Très faible</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Weak</source>
        <translation>Faible</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Moderate</source>
        <translation>Modérée</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Strong</source>
        <translation>Forte</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Very strong</source>
        <translation>Très forte</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="164"/>
        <source>Password strength: %1</source>
        <translation>Fiabilité du mot de passe : %1</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Hide the password</source>
        <translation>Cacher le mot de passe</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Show the password</source>
        <translation>Afficher le mot de passe</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="50"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <location filename="about.cpp" line="61"/>
        <source>Changelog</source>
        <translation>Journal des modifications</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <location filename="main.cpp" line="73"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Vous êtes apparemment connecté en tant que root, veuillez vous déconnecter et vous connecter en tant qu’utilisateur normal pour utiliser ce programme.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>You must run this program with admin access.</source>
        <translation>Vous devez exécuter ce programme avec un accès administrateur.</translation>
    </message>
</context>
</TS>