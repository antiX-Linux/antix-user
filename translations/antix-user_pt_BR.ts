<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="pt_BR">
<context>
    <name>MEConfig</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="973"/>
        <source>User Manager</source>
        <translation>Gerenciador de Usuários</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <location filename="ui_mainwindow.h" line="1031"/>
        <source>Administration</source>
        <translation>Administração</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <location filename="ui_mainwindow.h" line="1033"/>
        <source>Add a new user</source>
        <translation>Adicionar um novo usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <location filename="ui_mainwindow.h" line="974"/>
        <source>Add User Account</source>
        <translation>Adicionar Uma Conta de Usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <location filename="ui_mainwindow.h" line="976"/>
        <source>Enter password for new user</source>
        <translation>Insira a senha para o novo usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <location filename="ui_mainwindow.h" line="979"/>
        <source>password</source>
        <translation>senha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <location filename="ui_mainwindow.h" line="981"/>
        <source>Reenter password for new user</source>
        <translation>Insira novamente a mesma senha para o novo usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="984"/>
        <source>confirm password</source>
        <translation>Confirme a senha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <location filename="ui_mainwindow.h" line="986"/>
        <source>Username of new user</source>
        <translation>Nome de usuário para o novo usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="ui_mainwindow.h" line="988"/>
        <source>User login name:</source>
        <translation>Nome de autenticação do usuário:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <location filename="mainwindow.ui" line="466"/>
        <location filename="ui_mainwindow.h" line="990"/>
        <location filename="ui_mainwindow.h" line="1020"/>
        <source>Enter username of new user</source>
        <translation>Insira o nome de usuário do novo usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="993"/>
        <location filename="ui_mainwindow.h" line="1023"/>
        <source>username</source>
        <translation>nomedeusuario</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="995"/>
        <location filename="ui_mainwindow.h" line="999"/>
        <source>Password for new user</source>
        <translation>Senha para o novo usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <location filename="ui_mainwindow.h" line="997"/>
        <source>Confirm user password:</source>
        <translation>Confirme a senha do usuário:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="ui_mainwindow.h" line="1001"/>
        <source>User password:</source>
        <translation>Senha do usuário:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <location filename="ui_mainwindow.h" line="1002"/>
        <source>Grant this user administrative rights to the system (sudo)</source>
        <translation>Conceder a este usuário os direitos de administrador (sudo) do sistema operacional</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="1003"/>
        <source>Delete User Account</source>
        <translation>Excluir Uma Conta de Usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="333"/>
        <location filename="ui_mainwindow.h" line="1005"/>
        <source>Select user</source>
        <translation>Selecione o usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <location filename="mainwindow.ui" line="1436"/>
        <location filename="ui_mainwindow.h" line="1009"/>
        <location filename="ui_mainwindow.h" line="1147"/>
        <source>Select user to delete</source>
        <translation>Selecione o usuário a ser excluído</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="1011"/>
        <source>User to delete:</source>
        <translation>Usuário que será excluído:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="374"/>
        <location filename="ui_mainwindow.h" line="1013"/>
        <source>Also delete the user&apos;s home directory</source>
        <translation>Excluir também a pasta pessoal ou diretório ‘home’ do usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="377"/>
        <location filename="ui_mainwindow.h" line="1015"/>
        <source>Delete user home directory</source>
        <translation>Excluir a pasta pessoal ou diretório ‘home’ do usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="1016"/>
        <source>Rename User Account</source>
        <translation>Renomear Uma Conta de Usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="mainwindow.ui" line="570"/>
        <location filename="ui_mainwindow.h" line="1017"/>
        <location filename="ui_mainwindow.h" line="1029"/>
        <source>Select user to modify:</source>
        <translation>Selecione o usuário para ser modificado:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="450"/>
        <location filename="ui_mainwindow.h" line="1018"/>
        <source>New user name:</source>
        <translation>Nome do novo usuário:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <location filename="ui_mainwindow.h" line="1024"/>
        <source>Change User Password</source>
        <translation>Alterar a Senha de Usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="519"/>
        <location filename="ui_mainwindow.h" line="1026"/>
        <source>new password</source>
        <translation>nova senha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <location filename="ui_mainwindow.h" line="1027"/>
        <source>Confirm new password:</source>
        <translation>Confirme a nova senha:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <location filename="ui_mainwindow.h" line="1028"/>
        <source>confirm new password</source>
        <translation>confirme a nova senha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <location filename="ui_mainwindow.h" line="1030"/>
        <source>New user password:</source>
        <translation>Nova senha do usuário:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="1067"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <location filename="ui_mainwindow.h" line="1069"/>
        <source>Repair a user configuration</source>
        <translation>Reparar uma configuração de usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <location filename="mainwindow.ui" line="1393"/>
        <location filename="ui_mainwindow.h" line="1035"/>
        <location filename="ui_mainwindow.h" line="1145"/>
        <source>Modify User Account</source>
        <translation>Modificar Uma Conta de Usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <location filename="mainwindow.ui" line="685"/>
        <location filename="ui_mainwindow.h" line="1037"/>
        <location filename="ui_mainwindow.h" line="1041"/>
        <source>Select user to repair</source>
        <translation>Selecione o usuário para ser reparado</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <location filename="mainwindow.ui" line="1445"/>
        <location filename="ui_mainwindow.h" line="1039"/>
        <location filename="ui_mainwindow.h" line="1149"/>
        <source>User to change:</source>
        <translation>Mudar de usuário :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="717"/>
        <location filename="ui_mainwindow.h" line="1043"/>
        <source>Restore Defaults</source>
        <translation>Restaurar os Padrões</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="738"/>
        <location filename="ui_mainwindow.h" line="1045"/>
        <source>Restore browser configs to antiX defaults</source>
        <translation>Restaurar as configurações do navegador para os padrões do antiX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <location filename="ui_mainwindow.h" line="1050"/>
        <source>Mozilla (Iceweasel or Firefox) configs</source>
        <translation>Configurações para o Mozilla (Iceweasel ou Firefox)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <location filename="ui_mainwindow.h" line="1052"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <location filename="ui_mainwindow.h" line="1055"/>
        <source>Restore group memberships to antiX defaults</source>
        <translation>Restaurar as associações dos grupos para os padrões do antiX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="760"/>
        <location filename="ui_mainwindow.h" line="1060"/>
        <source>Group memberships</source>
        <translation>Membros do grupo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="763"/>
        <location filename="ui_mainwindow.h" line="1062"/>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="773"/>
        <location filename="ui_mainwindow.h" line="1064"/>
        <source>Change Autologin Settings</source>
        <translation>Alterar as Configurações de Autenticação/Acesso Automático</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="779"/>
        <location filename="ui_mainwindow.h" line="1065"/>
        <source>Log in automatically</source>
        <translation>Iniciar a sessão automaticamente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="786"/>
        <location filename="ui_mainwindow.h" line="1066"/>
        <source>Require password to log in</source>
        <translation>Solicitar a senha para iniciar a sessão</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <location filename="ui_mainwindow.h" line="1123"/>
        <source>Copy/Sync</source>
        <translation>Copiar/Sincronizar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="828"/>
        <location filename="ui_mainwindow.h" line="1071"/>
        <source>Copy Between Desktops</source>
        <translation>Copiar Entre as Áreas de Trabalho</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <location filename="mainwindow.ui" line="922"/>
        <location filename="ui_mainwindow.h" line="1073"/>
        <location filename="ui_mainwindow.h" line="1087"/>
        <source>Select desktop to copy from</source>
        <translation>Selecione a área de trabalho para onde copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="1076"/>
        <source>Select to only copy files</source>
        <translation>Selecione para copiar apenas os arquivos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="871"/>
        <location filename="ui_mainwindow.h" line="1078"/>
        <source>Copy only</source>
        <translation>Apenas copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="887"/>
        <location filename="mainwindow.ui" line="900"/>
        <location filename="ui_mainwindow.h" line="1080"/>
        <location filename="ui_mainwindow.h" line="1083"/>
        <source>Select desktop to copy to</source>
        <translation>Selecione a área de trabalho para onde copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <location filename="ui_mainwindow.h" line="1085"/>
        <source>Copy to:</source>
        <translation>Copiar para:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="931"/>
        <location filename="ui_mainwindow.h" line="1089"/>
        <source>Copy from:</source>
        <translation>Copiar de:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="944"/>
        <location filename="ui_mainwindow.h" line="1091"/>
        <source>Select to copy and then delete differences</source>
        <translation>Selecione para copiar, em seguida, apague as diferenças</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="947"/>
        <location filename="ui_mainwindow.h" line="1093"/>
        <source>Sync</source>
        <translation>Sincronizar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <location filename="ui_mainwindow.h" line="1096"/>
        <source>Select to copy/sync Shared</source>
        <translation>Selecione para copiar/sincronizar os compartilhamentos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <location filename="ui_mainwindow.h" line="1098"/>
        <source>Shared folder</source>
        <translation>Pasta Compartilhada</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1009"/>
        <location filename="ui_mainwindow.h" line="1100"/>
        <source>Select to copy/sync entire home</source>
        <translation>Selecione para copiar/sincronizar toda a pasta pessoal ou diretório ‘home’</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1012"/>
        <location filename="ui_mainwindow.h" line="1102"/>
        <source>Entire home</source>
        <translation>Toda a pasta pessoal (home)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1028"/>
        <location filename="ui_mainwindow.h" line="1104"/>
        <source>Select to copy/sync the browser configuration</source>
        <translation>Selecione para copiar/sincronizar as configurações do navegador</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1031"/>
        <location filename="ui_mainwindow.h" line="1106"/>
        <source>Mozilla (Firefox or Iceweasel) configs</source>
        <translation>Configurações do Mozilla (Firefox ou Iceweasel)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1044"/>
        <location filename="ui_mainwindow.h" line="1108"/>
        <source>Select to copy/sync Documents</source>
        <translation>Selecione para copiar/sincronizar a pasta Documentos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <location filename="ui_mainwindow.h" line="1110"/>
        <source>Documents folder</source>
        <translation>Pasta Documentos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1054"/>
        <location filename="ui_mainwindow.h" line="1111"/>
        <source>What to copy/sync:</source>
        <translation>O que deve ser copiado/sincronizado:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1070"/>
        <location filename="ui_mainwindow.h" line="1112"/>
        <source>Progress</source>
        <translation>Progresso</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1100"/>
        <location filename="mainwindow.ui" line="1116"/>
        <location filename="ui_mainwindow.h" line="1114"/>
        <location filename="ui_mainwindow.h" line="1117"/>
        <source>Status of the changes</source>
        <translation>Estado das alterações</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1119"/>
        <location filename="ui_mainwindow.h" line="1119"/>
        <source>Status:</source>
        <translation>Estado:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1135"/>
        <location filename="ui_mainwindow.h" line="1121"/>
        <source>Progress of the changes</source>
        <translation>Progresso das alterações</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1172"/>
        <location filename="ui_mainwindow.h" line="1144"/>
        <source>Add/Remove Groups</source>
        <translation>Adicionar/Remover os Grupos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1184"/>
        <location filename="ui_mainwindow.h" line="1124"/>
        <source>Add Group</source>
        <translation>Adicionar o Grupo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <location filename="ui_mainwindow.h" line="1126"/>
        <source>Enter name of new group</source>
        <translation>Insira o nome do novo grupo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1217"/>
        <location filename="ui_mainwindow.h" line="1129"/>
        <source>groupname</source>
        <translation>nomedogrupo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1227"/>
        <location filename="ui_mainwindow.h" line="1131"/>
        <source>Create a group with GID &gt; 1000</source>
        <translation>Criar um grupo com GID &gt; 1000</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1230"/>
        <location filename="ui_mainwindow.h" line="1133"/>
        <source>Create a user-level group</source>
        <translation>Criar um grupo com um GID no intervalo do usuário</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1259"/>
        <location filename="ui_mainwindow.h" line="1135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter name of new group&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insira o nome do novo grupo&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1262"/>
        <location filename="ui_mainwindow.h" line="1137"/>
        <source>Group name:</source>
        <translation>Nome do grupo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1281"/>
        <location filename="ui_mainwindow.h" line="1138"/>
        <source>Delete Group</source>
        <translation>Excluir o Grupo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1350"/>
        <location filename="ui_mainwindow.h" line="1140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select group to delete&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Selecione o grupo a ser excluído&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1359"/>
        <location filename="ui_mainwindow.h" line="1142"/>
        <source>Select group to delete:</source>
        <translation>Selecione o grupo a ser excluído:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1366"/>
        <location filename="ui_mainwindow.h" line="1143"/>
        <source>*Please doublecheck your selections before applying, removing a wrong group can break your system.</source>
        <translation>*Por favor, verifique as suas seleções antes de aplicar, porque se você excluir um grupo errado poderá danificar o seu sistema operacional.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1381"/>
        <location filename="ui_mainwindow.h" line="1155"/>
        <source>Group Membership</source>
        <translation>Membros do Grupo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1455"/>
        <location filename="ui_mainwindow.h" line="1150"/>
        <source>Groups user belongs to (change the groups by selecting/deselecting the appropriate boxes):</source>
        <translation>O usuário pertence aos grupos (altere os grupos marcando/desmarcando as caixas de opções apropriadas):</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1478"/>
        <location filename="ui_mainwindow.h" line="1151"/>
        <source>*Please doublecheck your selections before applying, assigning wrong group memberships can break your system. If you made a mistake, use restore group membership in Options tab to restore the defaults.</source>
        <translation>*Por favor, verifique as suas seleções antes de aplicá-las, se você atribuir associações erradas dos membros do grupo poderá danificar o seu sistema operacional. Se você cometer algum erro, utilize a opção “Restaurar as associações dos grupos para os padrões do antiX” que está disponível na aba ou guia Opções.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <location filename="ui_mainwindow.h" line="1153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select user to change&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Selecione o usuário a ser modificado&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1583"/>
        <location filename="ui_mainwindow.h" line="1157"/>
        <source>Cancel any changes then quit</source>
        <translation>Cancelar as modificações e sair</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1586"/>
        <location filename="ui_mainwindow.h" line="1159"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1593"/>
        <location filename="ui_mainwindow.h" line="1161"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1634"/>
        <location filename="ui_mainwindow.h" line="1165"/>
        <source>Display help </source>
        <translation>Exibir ajuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1637"/>
        <location filename="ui_mainwindow.h" line="1167"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1644"/>
        <location filename="ui_mainwindow.h" line="1169"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1651"/>
        <location filename="ui_mainwindow.h" line="1172"/>
        <source>Apply any changes</source>
        <translation>Aplicar as alterações</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1654"/>
        <location filename="ui_mainwindow.h" line="1174"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1661"/>
        <location filename="ui_mainwindow.h" line="1176"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1668"/>
        <location filename="ui_mainwindow.h" line="1179"/>
        <source>About this application</source>
        <translation>Sobre este aplicativo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="ui_mainwindow.h" line="1181"/>
        <source>About...</source>
        <translation>Sobre...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1678"/>
        <location filename="ui_mainwindow.h" line="1183"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="166"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="201"/>
        <source>none</source>
        <translation>nenhum</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <source>browse...</source>
        <translation>percorrer...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>The user configuration will be repaired. Please close all other applications now. When finished, please logout or reboot. Are you sure you want to repair now?</source>
        <translation>As configurações do usuário serão reparadas. Por favor, feche todos os outros aplicativos agora. Quando você concluir, saia da sessão ou reinicie. Você tem certeza de que quer reparar agora?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>User group membership was restored.</source>
        <translation>A associação ao grupo dos usuários foi restaurada.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Mozilla settings were reset.</source>
        <translation>As configurações do Mozilla foram redefinidas.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Autologin options</source>
        <translation>Opções de acesso automático</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="257"/>
        <source>Autologin has been disabled for the &apos;%1&apos; account.</source>
        <translation>O acesso automático foi desativado para a conta ‘%1’.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Autologin has been enabled for the &apos;%1&apos; account.</source>
        <translation>O acesso automático da sessão foi ativado para a conta do(a) usuário(a) ‘%1’.</translation>
    </message>
</context>
<context>
    <name>PassEdit</name>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Negligible</source>
        <translation>Insignificante</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Very weak</source>
        <translation>Muito fraca</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Weak</source>
        <translation>Fraca</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Moderate</source>
        <translation>Moderada</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Strong</source>
        <translation>Forte</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Very strong</source>
        <translation>Muito forte</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="164"/>
        <source>Password strength: %1</source>
        <translation>Nível de segurança da senha: %1</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Hide the password</source>
        <translation>Ocultar a senha</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Show the password</source>
        <translation>Exibir a senha</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="50"/>
        <source>License</source>
        <translation>Licença</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <location filename="about.cpp" line="61"/>
        <source>Changelog</source>
        <translation>Relatório de Alterações</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <location filename="main.cpp" line="73"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Ao que parece, você está acessando com o usuário ‘root’. Por favor, saia da sessão e entre novamente com o usuário normal para utilizar este programa.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>You must run this program with admin access.</source>
        <translation>Você tem que executar este programa com o usuário administrador.</translation>
    </message>
</context>
</TS>