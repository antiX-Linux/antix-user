<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ja">
<context>
    <name>MEConfig</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="973"/>
        <source>User Manager</source>
        <translation>ユーザマネージャ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <location filename="ui_mainwindow.h" line="1031"/>
        <source>Administration</source>
        <translation>管理者</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <location filename="ui_mainwindow.h" line="1033"/>
        <source>Add a new user</source>
        <translation>新規ユーザの追加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <location filename="ui_mainwindow.h" line="974"/>
        <source>Add User Account</source>
        <translation>ユーザアカウントの追加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <location filename="ui_mainwindow.h" line="976"/>
        <source>Enter password for new user</source>
        <translation>新規ユーザのパスワードを入れてください</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <location filename="ui_mainwindow.h" line="979"/>
        <source>password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <location filename="ui_mainwindow.h" line="981"/>
        <source>Reenter password for new user</source>
        <translation>新規ユーザのパスワードを再入力</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="182"/>
        <location filename="ui_mainwindow.h" line="984"/>
        <source>confirm password</source>
        <translation>パスワードを確認してください</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <location filename="ui_mainwindow.h" line="986"/>
        <source>Username of new user</source>
        <translation>新規ユーザのユーザ名</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <location filename="ui_mainwindow.h" line="988"/>
        <source>User login name:</source>
        <translation>ユーザのログイン名:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="217"/>
        <location filename="mainwindow.ui" line="466"/>
        <location filename="ui_mainwindow.h" line="990"/>
        <location filename="ui_mainwindow.h" line="1020"/>
        <source>Enter username of new user</source>
        <translation>新規ユーザのユーザ名を入れてください</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <location filename="mainwindow.ui" line="472"/>
        <location filename="ui_mainwindow.h" line="993"/>
        <location filename="ui_mainwindow.h" line="1023"/>
        <source>username</source>
        <translation>ユーザ名</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="995"/>
        <location filename="ui_mainwindow.h" line="999"/>
        <source>Password for new user</source>
        <translation>新規ユーザのパスワード</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <location filename="ui_mainwindow.h" line="997"/>
        <source>Confirm user password:</source>
        <translation>ユーザパスワードの確認:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="ui_mainwindow.h" line="1001"/>
        <source>User password:</source>
        <translation>ユーザのパスワード:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="271"/>
        <location filename="ui_mainwindow.h" line="1002"/>
        <source>Grant this user administrative rights to the system (sudo)</source>
        <translation>このユーザにシステムの管理者権限を与える (sudo)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="ui_mainwindow.h" line="1003"/>
        <source>Delete User Account</source>
        <translation>ユーザアカウントの削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="333"/>
        <location filename="ui_mainwindow.h" line="1005"/>
        <source>Select user</source>
        <translation>ユーザを選択します</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <location filename="mainwindow.ui" line="1436"/>
        <location filename="ui_mainwindow.h" line="1009"/>
        <location filename="ui_mainwindow.h" line="1147"/>
        <source>Select user to delete</source>
        <translation>削除するユーザを選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="1011"/>
        <source>User to delete:</source>
        <translation>削除するユーザ:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="374"/>
        <location filename="ui_mainwindow.h" line="1013"/>
        <source>Also delete the user&apos;s home directory</source>
        <translation>ユーザのホームディレクトリも削除してください</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="377"/>
        <location filename="ui_mainwindow.h" line="1015"/>
        <source>Delete user home directory</source>
        <translation>ユーザのホームディレクトリの削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="1016"/>
        <source>Rename User Account</source>
        <translation>ユーザアカウントの名前変更</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="mainwindow.ui" line="570"/>
        <location filename="ui_mainwindow.h" line="1017"/>
        <location filename="ui_mainwindow.h" line="1029"/>
        <source>Select user to modify:</source>
        <translation>変更するユーザを選択します</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="450"/>
        <location filename="ui_mainwindow.h" line="1018"/>
        <source>New user name:</source>
        <translation>新しいユーザ名</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <location filename="ui_mainwindow.h" line="1024"/>
        <source>Change User Password</source>
        <translation>ユーザパスワードの変更</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="519"/>
        <location filename="ui_mainwindow.h" line="1026"/>
        <source>new password</source>
        <translation>新規パスワード</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <location filename="ui_mainwindow.h" line="1027"/>
        <source>Confirm new password:</source>
        <translation>新しいパスワードの確認</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <location filename="ui_mainwindow.h" line="1028"/>
        <source>confirm new password</source>
        <translation>新しいパスワードの確認</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="586"/>
        <location filename="ui_mainwindow.h" line="1030"/>
        <source>New user password:</source>
        <translation>新規ユーザのパスワード</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="1067"/>
        <source>Options</source>
        <translation>オプション</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="623"/>
        <location filename="ui_mainwindow.h" line="1069"/>
        <source>Repair a user configuration</source>
        <translation>ユーザ設定の復旧</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <location filename="mainwindow.ui" line="1393"/>
        <location filename="ui_mainwindow.h" line="1035"/>
        <location filename="ui_mainwindow.h" line="1145"/>
        <source>Modify User Account</source>
        <translation>ユーザアカウントの変更</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="669"/>
        <location filename="mainwindow.ui" line="685"/>
        <location filename="ui_mainwindow.h" line="1037"/>
        <location filename="ui_mainwindow.h" line="1041"/>
        <source>Select user to repair</source>
        <translation>復旧するユーザの選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <location filename="mainwindow.ui" line="1445"/>
        <location filename="ui_mainwindow.h" line="1039"/>
        <location filename="ui_mainwindow.h" line="1149"/>
        <source>User to change:</source>
        <translation>変更するユーザ: </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="717"/>
        <location filename="ui_mainwindow.h" line="1043"/>
        <source>Restore Defaults</source>
        <translation>既定に戻す</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="738"/>
        <location filename="ui_mainwindow.h" line="1045"/>
        <source>Restore browser configs to antiX defaults</source>
        <translation>ブラウザーの設定を antiX の規定に戻します</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <location filename="ui_mainwindow.h" line="1050"/>
        <source>Mozilla (Iceweasel or Firefox) configs</source>
        <translation>Mozilla (Iceweasel または Firefox) 設定</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <location filename="ui_mainwindow.h" line="1052"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="754"/>
        <location filename="ui_mainwindow.h" line="1055"/>
        <source>Restore group memberships to antiX defaults</source>
        <translation>グループの資格を antiX の既定に戻します</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="760"/>
        <location filename="ui_mainwindow.h" line="1060"/>
        <source>Group memberships</source>
        <translation>グループメンバ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="763"/>
        <location filename="ui_mainwindow.h" line="1062"/>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="773"/>
        <location filename="ui_mainwindow.h" line="1064"/>
        <source>Change Autologin Settings</source>
        <translation>自動ログイン設定の変更</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="779"/>
        <location filename="ui_mainwindow.h" line="1065"/>
        <source>Log in automatically</source>
        <translation>自動ログイン</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="786"/>
        <location filename="ui_mainwindow.h" line="1066"/>
        <source>Require password to log in</source>
        <translation>ログイン時にパスワードを必要とする</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <location filename="ui_mainwindow.h" line="1123"/>
        <source>Copy/Sync</source>
        <translation>コピー・同期</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="828"/>
        <location filename="ui_mainwindow.h" line="1071"/>
        <source>Copy Between Desktops</source>
        <translation>デスクトップ間のコピー</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <location filename="mainwindow.ui" line="922"/>
        <location filename="ui_mainwindow.h" line="1073"/>
        <location filename="ui_mainwindow.h" line="1087"/>
        <source>Select desktop to copy from</source>
        <translation>デスクトップのコピー元を選択して下さい</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="1076"/>
        <source>Select to only copy files</source>
        <translation>コピーファイルを選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="871"/>
        <location filename="ui_mainwindow.h" line="1078"/>
        <source>Copy only</source>
        <translation>コピーのみ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="887"/>
        <location filename="mainwindow.ui" line="900"/>
        <location filename="ui_mainwindow.h" line="1080"/>
        <location filename="ui_mainwindow.h" line="1083"/>
        <source>Select desktop to copy to</source>
        <translation>デスクトップのコピー先を選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <location filename="ui_mainwindow.h" line="1085"/>
        <source>Copy to:</source>
        <translation>コピー先:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="931"/>
        <location filename="ui_mainwindow.h" line="1089"/>
        <source>Copy from:</source>
        <translation>コピー元:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="944"/>
        <location filename="ui_mainwindow.h" line="1091"/>
        <source>Select to copy and then delete differences</source>
        <translation>コピー・差分削除の選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="947"/>
        <location filename="ui_mainwindow.h" line="1093"/>
        <source>Sync</source>
        <translation>同期</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <location filename="ui_mainwindow.h" line="1096"/>
        <source>Select to copy/sync Shared</source>
        <translation>コピー・同期先を選択して下さい</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <location filename="ui_mainwindow.h" line="1098"/>
        <source>Shared folder</source>
        <translation>同期フォルダ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1009"/>
        <location filename="ui_mainwindow.h" line="1100"/>
        <source>Select to copy/sync entire home</source>
        <translation>コピー・同期するホーム全体の選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1012"/>
        <location filename="ui_mainwindow.h" line="1102"/>
        <source>Entire home</source>
        <translation>ホーム全体</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1028"/>
        <location filename="ui_mainwindow.h" line="1104"/>
        <source>Select to copy/sync the browser configuration</source>
        <translation>コピー・同期するブラウザ設定の選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1031"/>
        <location filename="ui_mainwindow.h" line="1106"/>
        <source>Mozilla (Firefox or Iceweasel) configs</source>
        <translation>Mozilla (Firefox または Iceweasel) 設定</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1044"/>
        <location filename="ui_mainwindow.h" line="1108"/>
        <source>Select to copy/sync Documents</source>
        <translation>コピー・同期ドキュメント先を選択</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <location filename="ui_mainwindow.h" line="1110"/>
        <source>Documents folder</source>
        <translation>ドキュメントフォルダ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1054"/>
        <location filename="ui_mainwindow.h" line="1111"/>
        <source>What to copy/sync:</source>
        <translation>コピー・同期について</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1070"/>
        <location filename="ui_mainwindow.h" line="1112"/>
        <source>Progress</source>
        <translation>進捗状況</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1100"/>
        <location filename="mainwindow.ui" line="1116"/>
        <location filename="ui_mainwindow.h" line="1114"/>
        <location filename="ui_mainwindow.h" line="1117"/>
        <source>Status of the changes</source>
        <translation>変更の状況</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1119"/>
        <location filename="ui_mainwindow.h" line="1119"/>
        <source>Status:</source>
        <translation>状況: </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1135"/>
        <location filename="ui_mainwindow.h" line="1121"/>
        <source>Progress of the changes</source>
        <translation>変更の進捗状況</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1172"/>
        <location filename="ui_mainwindow.h" line="1144"/>
        <source>Add/Remove Groups</source>
        <translation>グループの追加・削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1184"/>
        <location filename="ui_mainwindow.h" line="1124"/>
        <source>Add Group</source>
        <translation>グループの追加</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <location filename="ui_mainwindow.h" line="1126"/>
        <source>Enter name of new group</source>
        <translation>新しいグループ名を入れてください</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1217"/>
        <location filename="ui_mainwindow.h" line="1129"/>
        <source>groupname</source>
        <translation>グループ名</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1227"/>
        <location filename="ui_mainwindow.h" line="1131"/>
        <source>Create a group with GID &gt; 1000</source>
        <translation>GID &gt; 1000 のグループを作成</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1230"/>
        <location filename="ui_mainwindow.h" line="1133"/>
        <source>Create a user-level group</source>
        <translation>ユーザレベルのグループを作成</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1259"/>
        <location filename="ui_mainwindow.h" line="1135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter name of new group&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;新しいグループ名を入力してください&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1262"/>
        <location filename="ui_mainwindow.h" line="1137"/>
        <source>Group name:</source>
        <translation>グループ名:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1281"/>
        <location filename="ui_mainwindow.h" line="1138"/>
        <source>Delete Group</source>
        <translation>グループの削除</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1350"/>
        <location filename="ui_mainwindow.h" line="1140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select group to delete&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;削除するグループを選択&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1359"/>
        <location filename="ui_mainwindow.h" line="1142"/>
        <source>Select group to delete:</source>
        <translation>削除するグループを選択:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1366"/>
        <location filename="ui_mainwindow.h" line="1143"/>
        <source>*Please doublecheck your selections before applying, removing a wrong group can break your system.</source>
        <translation>*変更を適用する前に選択内容をもう一度確認してください。間違ったグループを削除すると、システムが壊れるおそれがあります。</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1381"/>
        <location filename="ui_mainwindow.h" line="1155"/>
        <source>Group Membership</source>
        <translation>グループメンバ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1455"/>
        <location filename="ui_mainwindow.h" line="1150"/>
        <source>Groups user belongs to (change the groups by selecting/deselecting the appropriate boxes):</source>
        <translation>ユーザ所属グループ (ボックス内の選択でグループの変更を行えます): </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1478"/>
        <location filename="ui_mainwindow.h" line="1151"/>
        <source>*Please doublecheck your selections before applying, assigning wrong group memberships can break your system. If you made a mistake, use restore group membership in Options tab to restore the defaults.</source>
        <translation>*選択を適用する前にもう一度見直してください。間違ったグループメンバーシップを割り当てると、システムが壊れてしまう可能性があります。間違えた場合は、オプションタブのグループメンバーシップの復元を使用して、既定に戻してください。</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <location filename="ui_mainwindow.h" line="1153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select user to change&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;変更するユーザを選択&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1583"/>
        <location filename="ui_mainwindow.h" line="1157"/>
        <source>Cancel any changes then quit</source>
        <translation>変更をキャンセルして終了</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1586"/>
        <location filename="ui_mainwindow.h" line="1159"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1593"/>
        <location filename="ui_mainwindow.h" line="1161"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1634"/>
        <location filename="ui_mainwindow.h" line="1165"/>
        <source>Display help </source>
        <translation>ヘルプ表示</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1637"/>
        <location filename="ui_mainwindow.h" line="1167"/>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1644"/>
        <location filename="ui_mainwindow.h" line="1169"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1651"/>
        <location filename="ui_mainwindow.h" line="1172"/>
        <source>Apply any changes</source>
        <translation>変更を適用</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1654"/>
        <location filename="ui_mainwindow.h" line="1174"/>
        <source>Apply</source>
        <translation>適用</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1661"/>
        <location filename="ui_mainwindow.h" line="1176"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1668"/>
        <location filename="ui_mainwindow.h" line="1179"/>
        <source>About this application</source>
        <translation>このアプリケーションについて</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="ui_mainwindow.h" line="1181"/>
        <source>About...</source>
        <translation>情報...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1678"/>
        <location filename="ui_mainwindow.h" line="1183"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="166"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="193"/>
        <location filename="mainwindow.cpp" line="201"/>
        <source>none</source>
        <translation>なし</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <source>browse...</source>
        <translation>ブラウズ中...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>The user configuration will be repaired. Please close all other applications now. When finished, please logout or reboot. Are you sure you want to repair now?</source>
        <translation>ユーザ設定を復旧します。全ての他アプリケーションを閉じてくさい。それが終わったら、ログアウトまたは再起動します。今すぐ復旧を行ってよろしいですか？</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>User group membership was restored.</source>
        <translation>ユーザグループのメンバシップが復旧しました</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>Mozilla settings were reset.</source>
        <translation>Mozilla の設定はリセットされました</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Autologin options</source>
        <translation>自動ログインオプション</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="257"/>
        <source>Autologin has been disabled for the &apos;%1&apos; account.</source>
        <translation>&apos;%1&apos; アカウントの自動ログインを無効にしました。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Autologin has been enabled for the &apos;%1&apos; account.</source>
        <translation>&apos;%1&apos; アカウントの自動ログインを有効にしました。</translation>
    </message>
</context>
<context>
    <name>PassEdit</name>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Negligible</source>
        <translation>わずかな</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Very weak</source>
        <translation>非常に弱い</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="162"/>
        <source>Weak</source>
        <translation>弱い</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Moderate</source>
        <translation>ほど良い</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Strong</source>
        <translation>強い</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="163"/>
        <source>Very strong</source>
        <translation>非常に強い</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="164"/>
        <source>Password strength: %1</source>
        <translation>パスワードの強度: %1</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Hide the password</source>
        <translation>パスワードを隠す</translation>
    </message>
    <message>
        <location filename="passedit.cpp" line="200"/>
        <source>Show the password</source>
        <translation>パスワードを表示する</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="50"/>
        <source>License</source>
        <translation>ライセンス</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <location filename="about.cpp" line="61"/>
        <source>Changelog</source>
        <translation>更新履歴</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>閉じる(&amp;C)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <location filename="main.cpp" line="73"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>rootとしてログインしているようです。このプログラムを使用するには、一度ログアウトして通常のユーザとしてログインしてください。</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>You must run this program with admin access.</source>
        <translation>このプログラムは管理者権限で実行してください。</translation>
    </message>
</context>
</TS>